// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Items/Bomb.h"
#include "GameFramework/Character.h"
#include "InThisTogetherCharacter.generated.h"

UCLASS(config = Game)
class AInThisTogetherCharacter : public ACharacter
{
	GENERATED_BODY()

	/** FPS camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Test, meta = (AllowPrivateAccess = "true"))
	class UTextRenderComponent* text;

public:
	AInThisTogetherCharacter();

	virtual void Tick(float DeltaTime) override;

	/** Get Current HP*/
	UFUNCTION(BlueprintPure, Category = Attributes)
	float getCurrHP();

	/** Get Current HP*/
	UFUNCTION(BlueprintPure, Category = Items)
	int getBombCount();

	/* Update Player's HP
	*@param hp updates player's currency hp by change var. can be +/-
	*/
	UFUNCTION(Category = Attributes)
	void updateHP(float change);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	/** Marks the properties we wish to replicate */
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	/** Applies damage to the character */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ABomb> Bomb_BP;

private:

	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Health, Category = Attributes)
	float Health;

	UPROPERTY(EditAnywhere, Category = Attributes)
	float MAX_HEALTH;

	UPROPERTY(EditAnywhere, Category = Items)
	int Key;

	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Bomb, Category = Items)
	int BombCount;

	/** Called when the Health variable gets updated */
	UFUNCTION()
	void OnRep_Health();

	/** Called when the BombCount variable gets updated */
	UFUNCTION()
	void OnRep_Bomb();

	/** Updates the character's text to match with the updated stats */
	void UpdateText();

	UFUNCTION()
	void initAttributes();
	void initHP(float initHP);
	void initMAX_HP(float maxHP);
	void initKeyCount(int keyCount);
	void initBombCount(int bombCount);

	/**
	* TakeDamage Server version. Call this instead of TakeDamage when you're a client
	*/
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTakeDamage(float dmg, FDamageEvent const & dmgEvent, AController * eventInstigator, AActor * dmgCauser);

	/** Contains the actual implementation of the ServerTakeDamage function */
	void ServerTakeDamage_Implementation(float dmg, FDamageEvent const & dmgEvent, AController * eventInstigator, AActor * dmgCauser);

	/** Validates the client. If the result is false the client will be disconnected */
	bool ServerTakeDamage_Validate(float dmg, FDamageEvent const & dmgEvent, AController * eventInstigator, AActor * dmgCauser);

	//Bomb related functions

	/** Will try to spawn a bomb */
	void AttempToSpawnBomb();

	/** Returns true if we can throw a bomb */
	bool HasBombs() { return BombCount > 0; }

	/**
	* Spawns a bomb. Call this function when you're authorized to.
	* In case you're not authorized, use the ServerSpawnBomb function.
	*/
	void SpawnBomb();

	/**
	* SpawnBomb Server version. Call this instead of SpawnBomb when you're a client
	*/
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSpawnBomb();

	/** Contains the actual implementation of the ServerSpawnBomb function */
	void ServerSpawnBomb_Implementation();

	/** Validates the client. If the result is false the client will be disconnected */
	bool ServerSpawnBomb_Validate();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", Meta = (BlueprintProtected = true))
	TSubclassOf<class UUserWidget> PlayerHUDClass;

	UPROPERTY()
	class UUserWidget* CurrWidget;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFPSCamera() const { return FirstPersonCamera; }
};

