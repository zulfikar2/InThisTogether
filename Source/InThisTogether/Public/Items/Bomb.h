// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Bomb.generated.h"

UCLASS()
class INTHISTOGETHER_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomb();

	/** The static mesh of the comp */
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* mesh;

	/** The projectile movement comp */
	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* ProjectileMovementComp;

	/** Sphere comp used for collision. Movement component need a collision component as root to function properly */
	UPROPERTY(VisibleAnywhere)
	USphereComponent* SphereComp;

	/** The delay until explosion */
	UPROPERTY(EditAnywhere, Category = Bomb)
	float fuse = 2.0f;

	UPROPERTY(EditAnywhere, Category = Bomb)
	float explosionRad = 200.0f;

	UPROPERTY(EditAnywhere, Category = Bomb)
	float explosionDmg = 25.0f;

	/** The particle system of the explosion */
	UPROPERTY(EditAnywhere)
	UParticleSystem* ExplosionFX;

	UPROPERTY(EditAnywhere)
	FLinearColor materialColour = FLinearColor::Black;

	UPROPERTY(EditAnywhere)
	FLinearColor armedColour = FLinearColor::Red;

private:

	/** Marks the properties we wish to replicate */
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(ReplicatedUsing = OnRep_IsArmed)
	bool bIsArmed = false;

	/** Called when bIsArmed gets updated */
	UFUNCTION()
	void OnRep_IsArmed();

	/** Arms Bomb (Changes colour to armedColour) */
	UFUNCTION()
	void ArmBomb();

	/** Called when our bomb bounces */
	UFUNCTION()
	void OnProjectileBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity);

	/** Performs an explosion after a certain amount of time */
	void PerformDelayedExplosion(float ExplosionDelay);

	/** Performs an explosion when called */
	UFUNCTION()
	void Explode();

	/**
	* The multicast specifier, indicates that every client will call
	*/
	UFUNCTION(Reliable, NetMulticast)
	void SimulateExplosionFX();

	/** The actual implementation of the SimulateExplosionFX */
	void SimulateExplosionFX_Implementation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
