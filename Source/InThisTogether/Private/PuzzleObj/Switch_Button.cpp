// Fill out your copyright notice in the Description page of Project Settings.

#include "Switch_Button.h"


// Sets default values
ASwitch_Button::ASwitch_Button()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASwitch_Button::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASwitch_Button::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

