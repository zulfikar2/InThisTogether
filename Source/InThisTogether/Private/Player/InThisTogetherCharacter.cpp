// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "InThisTogetherCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

//////////////////////////////////////////////////////////////////////////
// AInThisTogetherCharacter

AInThisTogetherCharacter::AInThisTogetherCharacter() {

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	/*bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;*/

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a FPS camera
	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCamera->SetupAttachment(GetCapsuleComponent());
	FirstPersonCamera->RelativeLocation = FVector(25.f, 5.f, 60.f); // Position the camera
	FirstPersonCamera->bUsePawnControlRotation = true;

	text = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Text"));
	text->SetupAttachment(GetCapsuleComponent());
	text->RelativeLocation = FVector(25.f, 5.f, 100.f); // Position the text
	text->SetWorldSize(30.0f);
	
	// Set bomb blueprint
	static ConstructorHelpers::FClassFinder<ABomb> BombBPClass(TEXT("/Game/Items/Bomb_BP"));
	if (BombBPClass.Class != NULL)
	{
		Bomb_BP = BombBPClass.Class;
	}

	// Set UI blueprint
	static ConstructorHelpers::FClassFinder<UUserWidget> UIClass(TEXT("/Game/UI/UI_HUD"));
	if (UIClass.Class != NULL)
	{
		PlayerHUDClass = UIClass.Class;
	}

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AInThisTogetherCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AInThisTogetherCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AInThisTogetherCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AInThisTogetherCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AInThisTogetherCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AInThisTogetherCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AInThisTogetherCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AInThisTogetherCharacter::OnResetVR);

	// Spawn Bomb
	PlayerInputComponent->BindAction("ThrowBomb", IE_Pressed, this, &AInThisTogetherCharacter::AttempToSpawnBomb);
}

void AInThisTogetherCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AInThisTogetherCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AInThisTogetherCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AInThisTogetherCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AInThisTogetherCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AInThisTogetherCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AInThisTogetherCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

float AInThisTogetherCharacter::getCurrHP() {
	return Health;
}

int AInThisTogetherCharacter::getBombCount() {
	return BombCount;
}

void AInThisTogetherCharacter::updateHP(float change) {
	Health += change;



	/*float roundedHP = FMath::RoundHalfToZero(10.0 * Health) / 10.0;
	FFormatNamedArguments Arguments;
	Arguments.Add(TEXT("CurrHP"), FText::AsNumber(Health));
	FText hp_str = FText::Format(NSLOCTEXT("NameSpace", "ExampleFText", "You currently have {CurrentHealth} health left."), Arguments);
	text->SetText(hp_str);*/
}

void AInThisTogetherCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Tell the engine to call the OnRep_Health and OnRep_BombCount each time a variable changes
	DOREPLIFETIME(AInThisTogetherCharacter, Health);
	DOREPLIFETIME(AInThisTogetherCharacter, BombCount);
}

void AInThisTogetherCharacter::initAttributes() {
	//init Character data
	initHP(100.0f);
	initMAX_HP(100.0f);
	initKeyCount(0);
	initBombCount(3);
}

void AInThisTogetherCharacter::initHP(float initHP) {
	Health = initHP;
}
void AInThisTogetherCharacter::initMAX_HP(float maxHP) {
	MAX_HEALTH = maxHP;
}
void AInThisTogetherCharacter::initKeyCount(int keyCount) {
	Key = keyCount;
}
void AInThisTogetherCharacter::initBombCount(int bombCount) {
	BombCount = bombCount;
}

void AInThisTogetherCharacter::ServerTakeDamage_Implementation(float dmg, FDamageEvent const & dmgEvent, AController * eventInstigator, AActor * dmgCauser) {
	TakeDamage(dmg, dmgEvent, eventInstigator, dmgCauser);
}

bool AInThisTogetherCharacter::ServerTakeDamage_Validate(float dmg, FDamageEvent const & dmgEvent, AController * eventInstigator, AActor * dmgCauser) {
	//assume it validates
	return true;
}

void AInThisTogetherCharacter::AttempToSpawnBomb() {
	//if you have bombs and ur server spawn it, else ask server to spawn it
	if (HasBombs()) {
		//client
		if (Role < ROLE_Authority) {
			ServerSpawnBomb();
		}
		//server
		else {
			SpawnBomb();
		}
	}
}

void AInThisTogetherCharacter::SpawnBomb() {
	BombCount--;
	UpdateText();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	SpawnParameters.Owner = GetController();

	//Spawn the bomb
	GetWorld()->SpawnActor<ABomb>(Bomb_BP, GetActorLocation() + GetActorForwardVector() * 200, GetActorRotation(), SpawnParameters);
}

void AInThisTogetherCharacter::ServerSpawnBomb_Implementation() {
	SpawnBomb();
}

bool AInThisTogetherCharacter::ServerSpawnBomb_Validate() {
	return true;
}

void AInThisTogetherCharacter::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	//updateHP(-DeltaTime * 0.01f * MAX_HEALTH);
}

void AInThisTogetherCharacter::OnRep_Health() {
	UpdateText();
}

void AInThisTogetherCharacter::OnRep_Bomb() {
	UpdateText();
}

void AInThisTogetherCharacter::UpdateText()
{
	//Create a string that will display the health and bomb count values
	FString NewText = FString("Health: ") + FString::SanitizeFloat(Health) + FString(" Bomb Count: ") + FString::FromInt(BombCount);

	//Set the created string to the text render comp
	text->SetText(FText::FromString(NewText));
}

void AInThisTogetherCharacter::BeginPlay() {

	Super::BeginPlay();

	//init attributes
	initAttributes();
	UpdateText();

	if (PlayerHUDClass != nullptr) {
		CurrWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);

		if (CurrWidget != nullptr) {
			CurrWidget->AddToViewport();
		}
	}
}

float AInThisTogetherCharacter::TakeDamage(float Damage, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser) {
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	//Decrease the character's hp 

	Health -= Damage;
	if (Health <= 0) {
		Health = MAX_HEALTH;
	}

	//Call the update text on the local client
	//OnRep_Health will be called in every other client so the character's text
	//will contain a text with the right values
	UpdateText();

	return Health;
}
