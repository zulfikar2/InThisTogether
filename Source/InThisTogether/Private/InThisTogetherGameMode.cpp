// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "InThisTogetherGameMode.h"
#include "InThisTogetherCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "InThisTogetherCharacter.h"
#include "Blueprint/UserWidget.h"
#include "UI/Main_HUD.h"

void AInThisTogetherGameMode::BeginPlay() {
	Super::BeginPlay();

	/*if (PlayerHUDClass != nullptr) {
		CurrWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);

		if (CurrWidget != nullptr) {
			CurrWidget->AddToViewport();
		}
	}*/
}

AInThisTogetherGameMode::AInThisTogetherGameMode() {
	
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	HUDClass = AMain_HUD::StaticClass();
}
