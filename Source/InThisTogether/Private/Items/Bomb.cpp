// Fill out your copyright notice in the Description page of Project Settings.

#include "Items/Bomb.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "UObject/ConstructorHelpers.h"


// Sets default values
ABomb::ABomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComp = CreateDefaultSubobject<USphereComponent>(FName("SphereComp"));
	SphereComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	SetRootComponent(SphereComp);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Mesh"));
	mesh->SetupAttachment(SphereComp);
	
	ProjectileMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(FName("ProjectileMovementComp"));
	ProjectileMovementComp->bShouldBounce = true;

	//Since we need to replicate some functionality
	//for this actor, we need to mark it as true
	SetReplicates(true);
}

void ABomb::SimulateExplosionFX_Implementation() {
	if (ExplosionFX) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionFX, GetTransform(), true);
	}
}

// Called when the game starts or when spawned
void ABomb::BeginPlay() {
	Super::BeginPlay();
	
	//Register that function that will be called in any bounce event
	ProjectileMovementComp->OnProjectileBounce.AddDynamic(this, &ABomb::OnProjectileBounce);

	UMaterialInstanceDynamic* material = mesh->CreateAndSetMaterialInstanceDynamic(0);
	material->SetVectorParameterValue(FName("Color"), materialColour);
}

// Called every frame
void ABomb::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void ABomb::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABomb, bIsArmed);
}

void ABomb::OnRep_IsArmed() {
	if (!bIsArmed) {
		ArmBomb();
	}
}


void ABomb::ArmBomb() {
	//Chance the base color of the static mesh to red
	UMaterialInstanceDynamic* material = mesh->CreateAndSetMaterialInstanceDynamic(0);
	material->SetVectorParameterValue(FName("Color"), armedColour);
}

void ABomb::OnProjectileBounce(const FHitResult & ImpactResult, const FVector & ImpactVelocity) {
	ArmBomb();
	if (!bIsArmed && Role == ROLE_Authority) {
		bIsArmed = true;
		//ArmBomb();
		PerformDelayedExplosion(fuse);
	}
}

void ABomb::PerformDelayedExplosion(float ExplosionDelay) {
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDel;
	TimerDel.BindUFunction(this, FName("Explode"));

	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, ExplosionDelay, false);
}

void ABomb::Explode() {
	SimulateExplosionFX();

	//We won't use any specific damage types in our case
	TSubclassOf<UDamageType> DmgType;
	//Do not ignore any actors
	TArray<AActor*> IgnoreActors;

	//This will eventually call the TakeDamage function that we have overriden in the Character class
	UGameplayStatics::ApplyRadialDamage(GetWorld(), explosionDmg, GetActorLocation(), explosionRad, DmgType, IgnoreActors, this, GetInstigatorController());

	FTimerHandle TimerHandle;
	FTimerDelegate TimerDel;

	TimerDel.BindLambda([&]()
	{
		Destroy();
	});

	//Destroy the actor after 0.3 seconds. 
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, 0.21f, false);
}
